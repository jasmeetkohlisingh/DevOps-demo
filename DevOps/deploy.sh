set +x

checkPod()
{
	echo "$(kubectl get replicationcontroller app -n web-app)"
        var=$(kubectl get replicationcontroller app -n web-app | grep -i "app" | awk '{print $3 $4}')
	var1=`expr $var % 10`
	var=`expr $var / 10`
	#echo "var : $var  $var1"
	if [ $var == $var1 ]
	then
		return 0
	else
		return 1
	fi
}

deployApp()
{
	if [ $(kubectl get po $1 -n web-app | wc -l) -gt 0 ]
        then
		echo "deleting po $1"
		echo $(kubectl delete po $1 -n web-app)
		sleep 4
		i=0
		while [ $i -le 11 ]
		do
			echo "Check if all pods are running"
			checkPod 
			ret=$?
			echo "return val : $ret"
			if [ $ret == 0 ]
			then
				echo "breaking"
				break
			fi
			i=`expr $i + 1`
			sleep 10
		done
	else
		echo " Error: in deleting pod..."
	fi
}

echo $(kubectl get namespaces)
PODS=$(kubectl get po -n web-app | grep -i app | awk '{print $1}')
echo "pods : $PODS"

for pod in ${PODS[@]};
do
	deployApp $pod
done


